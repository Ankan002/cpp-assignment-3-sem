#include<iostream>

using namespace std;

template <typename T> class Stack {
    private:
        T *arr;
        int size, currPos;

    public:
        Stack(int size) {
            arr = new T[size];
            this->currPos = 0;
            this->size = size;
        }

        void push(T e) {
            if(currPos == size) {
                throw string("Overflow");
            }

            this->arr[this->currPos] = e;
            this->currPos++;
        }

       T pop() {
            if(currPos == 0) {
                throw string("Underflow");
            }

            this->currPos--;
            return this->arr[this->currPos];
        }
};

int main(){
    Stack<int> st(2);

    try {
        st.push(12);
        st.push(22);
        // st.push(27);
        cout << st.pop() << endl;
        cout << st.pop() << endl;
    } catch (string e) {
        cout << e << endl;
    }

    return 0;
}
