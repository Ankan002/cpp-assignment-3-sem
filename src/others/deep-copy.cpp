#include<iostream>

using namespace std;

class Stack {
    int *arr;
    int size, currPos;

    public:
        Stack(int size) {
            arr = new int[size];
            currPos = 0;
            this->size = size;
        }

        Stack(Stack &s) {
            size = s.size;
            currPos = s.currPos;

            // Shallow Copy
            // arr = s.arr

            // Deep Copy
            arr = new int[s.size];
            
            for(int i = 0; i<sizeof(s.arr); i++) {
                arr[i] = s.arr[i];
            }
        }

        void push(int e) {
            if(currPos == size) {
                throw string("Overflow");
            }

            this->arr[this->currPos] = e;
            this->currPos++;
        }

        int *getArr() {
            return arr;
        }

        int pop() {
            if(currPos == 0) {
                throw string("Underflow");
            }

            this->currPos--;
            return this->arr[this->currPos];
        }
};

int main() {
    Stack s(12);
    s.push(1);
    s.push(2);
    s.push(3);

    Stack z(s);
    z.push(4);

    s.push(11);

    cout << z.pop() << endl;

    return 0;
}
