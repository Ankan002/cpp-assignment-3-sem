#include<iostream>
#include<set>

using namespace std;

class Person {
    virtual int getId() = 0;
};

class Voter: public Person {
    bool voted;
    int id;
    string name;

    public:
        Voter(){}

        Voter(string n, int i) {
            id = i;
            name = n;
            voted = false;
        }

        int getId() {
            return id;
        }

        void toggleVoted() {
            voted = true;
        }

        bool getVoted() {
            return voted;
        }
};

class Candidate: public Person {
    int id, votes;
    string name;

    public:
        Candidate(){}

        Candidate(string n, int i) {
            id = i;
            name = n;
            votes = 0;
        }

        int getId() {
            return id;
        }

        void incrementVote() {
            votes++;
        }

        int getNumberOfVotes() {
            return votes;
        }
};

class Evm {
    vector<Candidate> candidates;
    vector<Voter> voters;

    Candidate winner;

    bool isEnded = false;
    bool isVotingStarted = false;

    int currentCId = 1;
    int currentVId = 1;

    public:
        void addCandidate(string name) {
            if(isVotingStarted) {
                throw string("Voting has stared");
            }

            Candidate c(name, currentCId);
            currentCId++;
            candidates.push_back(c);
        }

        void addVoter(string name) {
            if(isVotingStarted) {
                throw string("Voting has stared");
            }

            Voter v(name, currentVId);
            currentVId++;
            voters.push_back(v);
        }

        void startVoting() {
            isVotingStarted = true;
        }

        void pollVote(int voterId, int candidateId) {
            if(!isVotingStarted) {
                throw string("Voting not started");
            }

            if(isEnded) {
                throw string("Voting ended");
            }

            Voter foundVoter;
            Candidate foundCandidate;

            bool found = false;

            for(Voter v : voters) {
                if(v.getId() == voterId) {
                    foundVoter = v;
                    found = true;
                    break;
                }
            }

            if(!found) {
                throw string("No Voter found!!");
            }

            if(foundVoter.getVoted()) {
                throw string("Already Voted!!");
            }

            found = false;

            for(Candidate c : candidates) {
                if(c.getId() == candidateId) {
                    foundCandidate = c;
                    found = true;
                    break;
                }
            }

            if(!found) {
                throw string("No Candidate found found!!");
            }

            foundCandidate.incrementVote();
            foundVoter.toggleVoted();
        }
};
