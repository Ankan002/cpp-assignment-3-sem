#include <iostream>

using namespace std;

template <typename A, typename R> class Bill {
    public:
        virtual R getBillAmount(A units) = 0;
};

class ElectricityBill : public Bill<int, double> {
    double rate;

    public:
        ElectricityBill(double rate) {
            this->rate = rate;
        }

        double getBillAmount(int units) {
            return this->rate * units;
        }
};

class WaterBill : public Bill<double, double> {
    double rate;

    public:
        WaterBill(double rate) {
            this->rate = rate;
        }

        double getBillAmount(double units) {
            return this->rate * units;
        }
};

class TelephoneBill : public Bill<float, double> {
    double rate;

    public:
        TelephoneBill(double rate) {
            this->rate = rate;
        }

        double getBillAmount(float units) {
            return rate * units;
        }   
};

class GasBill : public Bill<int, double> {
    double rate;

    public:
        GasBill(double rate) {
            this->rate = rate;
        }

        double getBillAmount(int units) {
            return rate * units;
        } 
};

int main() {
    WaterBill wb(10.00);
    ElectricityBill eb(11.00);
    TelephoneBill tb(13.41);
    GasBill gb(100.31);

    cout << "Water Bill is: " << wb.getBillAmount(10.0) << endl;
    cout << "Electricity Bill is: " << eb.getBillAmount(20) << endl;
    cout << "Telephone Bill is: " << tb.getBillAmount(25.13) << endl;
    cout << "GasBill Bill is: " << gb.getBillAmount(41) << endl;

    return 0;
}
