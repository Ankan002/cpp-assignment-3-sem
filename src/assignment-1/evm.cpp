#include <iostream>
#include <set>
#include <uuid/uuid.h>

using namespace std;

class Person {
    public:
        virtual string getId() = 0;
};

class Candidate : Person {
    private:
        string id, name;
        set<string> voters;

    public:
        Candidate(){}

        Candidate(string id, string name) {
            this->id = id;
            this->name = name;
        }

        void addVote(string voterId) {
            this->voters.insert(voterId);
        }

        int getVotes() {
            return this->voters.size();
        }

        string getId(){
            return this->id;
        }

        string getName() {
            return this->name;
        }
};

class Voter : Person {
    private:
        string id, name;
        bool voted;

    public:
        Voter() {}

        Voter(string id, string name) {
            this->id = id;
            this->name = name;
        }

        void toggleVoted() {
            this->voted = true;
        }

        bool hasVoted() {
            return this->voted;
        }

        string getId(){
            return this->id;
        }
};

class EVM {
    private:
        vector<Candidate> candidates;
        vector<Voter> voterList;

        Candidate winner;

        bool hasVotingStarted;
        bool isResultDeclared;

    public:
        string addCandidate(string name) {
            if(this->hasVotingStarted){
                throw string("Voting has started");
            }

            uuid_t uuid;
            char id[37];
            uuid_generate_random(uuid);
            uuid_unparse(uuid, id);

            Candidate candidate(id, name);
            this->candidates.push_back(candidate);

            return id;
        }

        string addVoter(string name) {
            if(this->hasVotingStarted){
                throw string("Voting has started");
            }

            uuid_t uuid;
            char id[37];
            uuid_generate_random(uuid);
            uuid_unparse(uuid, id);

            Voter voter(id, name);
            this->voterList.push_back(voter);

            return id;
        }

        void vote(string voterId, string candidateId){
            if(this->isResultDeclared) {
                throw string("Voting has Ended");
            }

            if(!this->hasVotingStarted) {
                this->hasVotingStarted = true;
            }

            Voter foundVoter;
            Candidate foundCandidate;
            bool found = false;

            for(Voter voter : this->voterList){
                if(voter.getId() == voterId) {
                    foundVoter = voter;
                    found = true;
                }
            }

            if(!found) {
                throw string("No Voter Found!!");
            }

            if(foundVoter.hasVoted()) {
                throw string("You have already voted!!");
            }

            found = false;

            for(Candidate candidate : this->candidates) {
                if(candidate.getId() == candidateId) {
                    foundCandidate = candidate;
                    found = true;
                }
            }

            if(!found) {
                throw string("No Candidate Found!!");
            }

            foundCandidate.addVote(candidateId);
            foundVoter.toggleVoted();
        }

        void showCandidates() {
            for(Candidate candidate : this->candidates) {
                cout << candidate.getId() << ": " << candidate.getName() << endl;
            }
        }

        Candidate getWinner() {
            if(this->candidates.size() < 1) {
                throw string("No Candidate is there...");
            }

            if(this->isResultDeclared) {
                return this->winner;
            }

            int highestVoteNumber = -1;

            for(Candidate candidate : this->candidates) {
                if(candidate.getVotes() > highestVoteNumber) {
                    highestVoteNumber = candidate.getVotes();
                    this->winner = candidate;
                }
            }

            this->isResultDeclared = true;

            return this->winner;
        }
};

int main() {
    bool canContinue = true;

    EVM evm;

    cout << "*****************" << endl;
    cout << "EVM Command List: " << endl;
    cout << "1 -> Add new Candidate" << endl; 
    cout << "2 -> Add new Voter" << endl; 
    cout << "3 -> Poll Vote" << endl; 
    cout << "4 -> Get Winner" << endl; 
    cout << "5 -> Exit EVM" << endl; 
    cout << "*****************" << endl;
    cout << "" << endl;

    while(canContinue) {
        int actionNumber;

        cout << "Enter Action Number: " << endl;
        cin >> actionNumber;

        try {
            if (actionNumber == 1) {
                string candidateName;

                cout << "Enter candidate name:" << endl;
                cin >> candidateName;

                string candidateId = evm.addCandidate(candidateName);

                cout << "Your id is: " << candidateId << endl;
            } else if (actionNumber == 2) {
                string voterName;

                cout << "Enter voter name:" << endl;
                cin >> voterName;

                string voterId = evm.addVoter(voterName);

                cout << "Your id is: " << voterId << endl;
            } else if (actionNumber == 3) {
                cout << "---------" << endl;
                cout << "Candidates are: " << endl;
                evm.showCandidates();
                cout << "---------" << endl;

                string voterId, candidateId;

                cout << "Enter voter id: " << endl;
                cin >> voterId;

                cout << "Enter candidate id: " << endl;
                cin >> candidateId;

                evm.vote(voterId, candidateId);

                cout << "Vote added successfully!!" << endl;
            } else if (actionNumber == 4) {
                cout << "The WINNER of the Election is: " << evm.getWinner().getName() << endl;
            } else if (actionNumber == 5) {
                canContinue = false;
            } else {
                cout << "Choose a valid input" << endl;
            }
        } catch (string error) {
            cout << error << endl;
        }
    }

    return 0;
}
